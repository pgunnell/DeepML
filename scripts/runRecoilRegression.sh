#!/bin/bash

WORKDIR=`pwd`/regress_results
mkdir -p ${WORKDIR}

RED='\033[0;31m'
NC='\033[0m'

MODEL=$1
OUTDIR=$2/model_${MODEL}
CUT="isW==0"
if [ "$#" -ge 3 ]; then
    CUT=$3
fi

#setup environment
WORKPATH=/afs/cern.ch/work/p/pgunnell/WMassAnalysis/DeepML-Pedro
cd ${WORKPATH}
export PATH=/afs/cern.ch/work/p/pgunnell/WMassAnalysis/miniconda3/bin:$PATH
source activate deepjetLinux3
DEEPJETCORE=${WORKPATH}/../DeepJetCore
export PYTHONPATH=${WORKPATH}/Train:${WORKPATH}/modules:${DEEPJETCORE}/../:$PYTHONPATH
export LD_LIBRARY_PATH=${DEEPJETCORE}/compiled:$LD_LIBRARY_PATH
export PATH=${DEEPJETCORE}/bin:$PATH
export LD_PRELOAD=$CONDA_PREFIX/lib/libmkl_core.so:$CONDA_PREFIX/lib/libmkl_sequential.so
ulimit -s 65532

trainDataDir=${WORKDIR}/train_data
trainDir=${WORKDIR}/train
testDir=${WORKDIR}/test
predictDir=${WORKDIR}/predict

#prepare model
varList="tkmet_logpt,ntnpv_logpt,npvmet_logpt,tkmet_phi,tkmet_n"
varList="${varList},tkmet_sphericity,ntnpv_sphericity,npvmet_sphericity"
varList="${varList},absdphi_ntnpv_tk,dphi_puppi_tk"
varList="${varList},rho,nvert,mindz,vz"
if [[ "${MODEL}"   -ge "0" && "${MODEL}" -le "49" ]]; then
    toRegress="mu"
    if [[ "${MODEL}" -ge "6" && "${MODEL}" -le "11" ]]; then
        toRegress="mu,qm,qp";
    fi
    CLASSARGS="--sel ${CUT} --target lne1 --varList ${varList} --regress ${toRegress}" 
elif [[ "${MODEL}"   -ge "50" && "${MODEL}" -le "97" ]]; then
    CLASSARGS="--sel ${CUT} --target lne1 --varList ${varList} --regress mu,sigma,a1,a2"
elif [[ "${MODEL}"   -ge "98" && "${MODEL}" -le "99" ]]; then
    CLASSARGS="--sel ${CUT} --target lne1 --varList ${varList} --regress mu,sigma"
elif [[ "${MODEL}" -ge "100" && "${MODEL}" -le "149" ]]; then
    CLASSARGS="--sel ${CUT} --target e2 --varList ${varList} --regress mu_e2"
elif [[ "${MODEL}" -ge "150" && "${MODEL}" -le "199" ]]; then
    CLASSARGS="--sel ${CUT} --target e2 --varList ${varList} --regress mu_e2,sigma_e2,a1_e2,a2_e2,n_e2"
else
    echo "Unknown model ${MODEL}...."
    exit -1
fi

echo -e "Preparing data to train for model ${RED} ${MODEL} ${NC} with arguments ${RED} ${CLASSARGS} ${NC}"
rm -rf ${trainDataDir}
convertFromRoot.py -i data/recoil_file_list.txt -o ${trainDataDir} -c TrainData_Recoil --noRelativePaths --classArgs "${CLASSARGS}"
echo "Training model"
rm -rf ${trainDir}
python Train/test_TrainData_Recoil.py ${trainDataDir}/dataCollection.dc ${trainDir} --modelMethod ${MODEL}
echo "Testing model"
rm -rf ${testDir} 
convertFromRoot.py --testdatafor ${trainDir}/trainsamples.dc -i data/recoil_file_list.txt -o ${testDir} --noRelativePaths
echo "Running predictions now"
rm -rf ${predictDir}
predict.py ${trainDir}/KERAS_model.h5  ${testDir}/dataCollection.dc ${predictDir}

#prepare output directories
if [[ -z "$OUTDIR" ]]; then
    OUTDIR="recoil-regression-$(uuidgen)"
fi
mkdir -p $OUTDIR
echo -e "Output directory set to ${RED} $OUTDIR ${NC}"
mv -v ${WORKDIR}/* ${OUTDIR}/
rm -rf ${WORKDIR}
echo -e "Results have been copied to ${OUTDIR}"

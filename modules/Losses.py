import tensorflow as tf
from keras import backend as K
import math
from numpy import sqrt,arange
from scipy.special import erf

from NumericalTools import rangeTransform

def gd_loss(y_true, y_pred,sess=None):

    """customized loss function for a gauss+double expo tails """

    y_true = tf.unstack(y_true, axis=1, num=1)[0]
    
    #mu    = tf.unstack(y_pred, axis=1)[0]
    #sigma = tf.clip_by_value( tf.unstack(y_pred, axis=1)[1], 0.01, 9e12)
    #a1    = tf.clip_by_value( tf.unstack(y_pred, axis=1)[2], 0, 9e12)
    #a2    = tf.clip_by_value( tf.unstack(y_pred, axis=1)[3], 0, 9e12)

    mu    = rangeTransform( tf.unstack(y_pred, axis=1)[0], -3, 3)
    sigma = rangeTransform( tf.clip_by_value(tf.unstack(y_pred, axis=1)[1],0,9e12),  0, 5)
    a1    = rangeTransform( tf.clip_by_value(tf.unstack(y_pred, axis=1)[2],0,9e12),  0, 5)
    a2    = rangeTransform( tf.clip_by_value(tf.unstack(y_pred, axis=1)[3],0,9e12),  0, 5)

    t = (y_true - mu)/sigma
    
    Norm = sqrt(math.pi/2)*sigma*(tf.erf(a2/sqrt(2)) - tf.erf(-a1/sqrt(2))) 
    Norm += K.exp(-0.5*K.pow(a1,2))*sigma/a1 
    Norm += K.exp(-0.5*K.pow(a2,2))*sigma/a2
    
    aux = tf.where(K.greater_equal(t, a2),
                   K.log(Norm) -0.5*K.pow(a2, 2) + a2*t,
                   tf.where(K.greater_equal(t, -a1),
                            K.log(Norm) + 0.5*K.pow(t,2),
                            K.log(Norm) -0.5*K.pow(a1, 2) - a1*t
                        )
    )

    if sess:
        print(sess.run(aux))
    return K.sum(aux)


def gd_offset_loss(y_true, y_pred,sess=None):

    """customized loss for a gauss+double expo tails + offset """

    y_true   = tf.unstack(y_true, axis=1, num=1)[0]

    e2       = tf.unstack(y_pred, axis=1)[0]
    sigma_e2 = tf.unstack(y_pred, axis=1)[1]
    a1_e2    = tf.unstack(y_pred, axis=1)[2]
    a2_e2    = tf.unstack(y_pred, axis=1)[3]
    n_e2     = tf.unstack(y_pred, axis=1)[4]

    t = (y_true - e2)/sigma_e2
    t1 = (math.pi + e2)/sigma_e2
    t2 = (math.pi - e2)/sigma_e2

    n1 = (sigma_e2/a1_e2)*K.exp(0.5*tf.pow(a1_e2,2))*(K.exp(-tf.pow(a1_e2,2)) - K.exp(-a1_e2*t1))
    n2 = (sigma_e2/a2_e2)*K.exp(0.5*tf.pow(a2_e2,2))*(K.exp(-tf.pow(a2_e2,2)) - K.exp(-a2_e2*t2))

    N = tf.where(tf.logical_and(tf.greater_equal(a1_e2, t1), tf.greater_equal(a2_e2, t2)),
                 sqrt(math.pi/2)*sigma_e2*(tf.erf(t2/sqrt(2)) - tf.erf(-t1/sqrt(2))),
                 tf.where(tf.logical_and(tf.greater(t1, a1_e2), tf.greater_equal(a2_e2, t2)),
                          sqrt(math.pi/2)*sigma_e2*(tf.erf(t2/sqrt(2)) - tf.erf(-a1_e2/sqrt(2))) + n1,
                          tf.where(tf.logical_and(tf.greater_equal(a1_e2, t1), tf.greater(t2, a2_e2)),
                                   sqrt(math.pi/2)*sigma_e2*(tf.erf(a2_e2/sqrt(2)) - tf.erf(-t1/sqrt(2))) + n2,
                                   sqrt(math.pi/2)*sigma_e2*(tf.erf(a2_e2/sqrt(2)) - tf.erf(-a1_e2/sqrt(2))) + n1 + n2
                               )
                      )
             )
        
    f = tf.where(tf.greater_equal(t, a2_e2),
                 K.exp(0.5*tf.pow(a2_e2, 2) - a2_e2*t),
                 tf.where(tf.greater_equal(t, -a1_e2),
                          K.exp(-0.5*tf.pow(t,2)),
                          K.exp(0.5*tf.pow(a1_e2, 2) + a1_e2*t)
                      )
    )

    aux = -K.log(n_e2 + f*(1-2*math.pi*n_e2)/N)
    aux = tf.where(tf.is_nan(aux), 500*tf.ones_like(aux), aux)
    aux = tf.where(tf.is_inf(aux), 500*tf.ones_like(aux), aux)

    if sess:
        print(sess.run(aux))
    return K.sum(aux)


def logcosh(y_true, y_pred):
    """logcosh losse should be implemented in newer Keras versions"""
    def _logcosh(x):
        return x + K.softplus(-2. * x) - K.log(2.)
    return K.mean(_logcosh(y_pred - y_true), axis=-1)

def quantile_loss_generator(q):
    """generator for a q-quantile loss"""
    def aux_fun(y_true, y_pred):
        e = (y_true - y_pred)
        aux = tf.where(tf.greater_equal(e, 0.), q*e, (q-1)*e)
        return K.mean(aux)
    qkey='%3.2f'%q
    aux_fun.__name__ = 'quantile_loss_'+qkey.replace(".", "")
    return aux_fun


def huber(y_true,y_pred,delta=1.0):
    """Huber loss"""
    z = K.abs(y_true[:,0] - y_pred[:,0])
    mask = K.cast(K.less(z,delta),K.floatx())
    return K.mean( 0.5*mask*K.square(z) + (1.-mask)*(delta*z - 0.5*delta**2) )

def ahuber(y_true,y_pred,dm=0.5,dp=1.0):
    """asymmetric Huber loss"""
    z = y_true[:,0] - y_pred[:,0]
    aux = tf.where( tf.greater_equal(z,dp),
                    dp*z-0.5*dp**2,
                    tf.where( tf.greater_equal(z,-dm),
                              0.5*K.square(z),
                              -dm*z-0.5*dm**2) 
                )
    return K.mean(aux)

def ahuber_q(y_true,y_pred,dm=0.5,dp=1.0,qm=0.16,qp=0.84):
    """asymmetric Huber loss with quantiles"""

    mu_pred  = tf.unstack(y_pred, axis=1)[0]
    e_mu   = (y_true-mu_pred)
    aux_mu = tf.where( tf.greater_equal(e_mu,dp),
                       dp*e_mu-0.5*dp**2,
                       tf.where( tf.greater_equal(e_mu,-dm),
                                 0.5*K.square(e_mu),
                                 -dm*e_mu-0.5*dm**2) )
    aux_mu = K.mean(aux_mu)
                
    qm_pred  = tf.unstack(y_pred, axis=1)[1]
    em = (y_true-qm_pred)
    aux_m = tf.where(tf.greater_equal(em, 0.), qm*em, (qm-1)*em)
    aux_m = K.mean(aux_m)

    qp_pred  = tf.unstack(y_pred, axis=1)[2]
    ep = (y_true-qp_pred)
    aux_p = tf.where(tf.greater_equal(ep, 0.), qp*ep, (qp-1)*ep)
    aux_p = K.mean(aux_p)

    return aux_m+aux_p+aux_mu
    
#slightly modified gd_loss functions 
def gd_loss_sigm_gauss(y_true, y_pred):

    """customized loss function for a gauss+double expo tails, using gaussians to constrain the fitted parameters and a sigmoid in the regression of the mean and the width """

    y_true = tf.unstack(y_true, axis=1, num=1)[0]

    mu    = tf.unstack(y_pred, axis=1)[0]
    sigma = tf.clip_by_value(tf.unstack(y_pred, axis=1)[1],1e-5,9e12)
    a1    = tf.clip_by_value(tf.unstack(y_pred, axis=1)[2],1e-5,9e12)
    a2    = tf.clip_by_value(tf.unstack(y_pred, axis=1)[3],1e-5,9e12)

    central_value_mu = 0.8 
    central_value_sigma = 0.8
    central_value_a1= 1.6
    central_value_a2= 1.6

    t = (y_true - central_value_mu*tf.sigmoid(mu))/(central_value_sigma*tf.sigmoid(sigma))


    Norm = sqrt(math.pi/2)*sigma*(tf.erf(a2/sqrt(2)) - tf.erf(-a1/sqrt(2))) 
    Norm += K.exp(-0.5*K.pow(a1,2))*sigma/a1 
    Norm += K.exp(-0.5*K.pow(a2,2))*sigma/a2
    
    Norm += 4*sqrt(1/2*math.pi)

    Norm  = tf.clip_by_value(Norm,1e-5,9e12)


    aux = tf.where(K.greater_equal(t, a2),
                   K.log(Norm) - 0.5*K.pow(a2, 2) + a2*t  + K.pow((a1-central_value_a1),2)/2 + K.pow((a2-central_value_a2),2)/2 + 0.5*K.pow(mu-central_value_mu,2) + 0.5*K.pow(sigma-central_value_sigma,2),
                   tf.where(K.greater_equal(t, -a1),
                            K.log(Norm) + 0.5*K.pow(t,2) + K.pow((a1-central_value_a1),2)/2 + K.pow((a2-central_value_a2),2)/2 + 0.5*K.pow(mu-central_value_mu,2) + 0.5*K.pow(sigma-central_value_sigma,2),
                            K.log(Norm) - 0.5*K.pow(a1, 2) - a1*t + K.pow((a2-central_value_a2),2)/2 + K.pow((a1-central_value_a1),2)/2 + 0.5*K.pow(mu-central_value_mu,2) + 0.5*K.pow(sigma-central_value_sigma,2),
                    )
    )

    return K.sum(aux)

def gd_loss_sigm_gauss_reduced(y_true, y_pred):

    """customized loss function for a gauss+double expo tails, using gaussians to constrain the fitted parameters and a sigmoid in th
e regression of the mean and the width and fixing the tail parameters"""

    y_true = tf.unstack(y_true, axis=1, num=1)[0]

    mu    = tf.unstack(y_pred, axis=1)[0]
    sigma = tf.clip_by_value(tf.unstack(y_pred, axis=1)[1],1e-5,9e12)

    a1    = 1.6
    a2    = 1.6

    central_value_mu = 0.8
    central_value_sigma = 0.8

    t = (y_true - central_value_mu*tf.sigmoid(mu))/(central_value_sigma*tf.sigmoid(sigma))


    Norm = sqrt(math.pi/2)*sigma*(erf(a2/sqrt(2)) - erf(-a1/sqrt(2)))
    Norm += K.exp(-0.5*K.pow(a1,2))*sigma/a1
    Norm += K.exp(-0.5*K.pow(a2,2))*sigma/a2

    Norm += 2*sqrt(1/2*math.pi)

    Norm  = tf.clip_by_value(Norm,1e-5,9e12)


    aux = tf.where(K.greater_equal(t, a2),
                   K.log(Norm) - 0.5*K.pow(a2, 2) + a2*t  + 0.5*K.pow(mu-central_value_mu,2) + 0.5*K.pow(sigma-central_value_sigma,2)
,
                   tf.where(K.greater_equal(t, -a1),
                            K.log(Norm) + 0.5*K.pow(t,2) + 0.5*K.pow(mu-central_value_mu,2) + 0.5*K.pow(sigma-central_value_sigma,2),
                            K.log(Norm) - 0.5*K.pow(a1, 2) - a1*t + 0.5*K.pow(mu-central_value_mu,2) + 0.5*K.pow(sigma-central_value_sigma,2),
                        )
               )

    return K.sum(aux)


# list of all the losses
global_loss_list={'gd_loss':gd_loss,
                  'gd_offset_loss':gd_offset_loss,
                  'gd_loss_sigm_gauss':gd_loss_sigm_gauss,
                  'gd_loss_sigm_gauss_reduced':gd_loss_sigm_gauss_reduced,
                  'logcosh':logcosh,
                  'huber':huber,
                  'ahuber':ahuber,
                  'ahuber_q':ahuber_q}

for q in arange(0.05,1.0,0.05):
    global_loss_list['%3.2f'%q]=quantile_loss_generator(q)

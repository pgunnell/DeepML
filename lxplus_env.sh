export PATH="/afs/cern.ch/work/p/pgunnell/WMassAnalysis/miniconda3/bin:$PATH"
export DEEPJETCORE=../DeepJetCore

THISDIR=`pwd`
cd $DEEPJETCORE/
source lxplus_env.sh
cd $THISDIR
export PYTHONPATH=`pwd`/modules:$PYTHONPATH
export DEEPJET=`pwd`

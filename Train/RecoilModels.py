from keras.models import Model
from keras.layers import Input, Dense, Dropout,BatchNormalization,Concatenate, concatenate, Add
from keras.layers.advanced_activations import LeakyReLU, PReLU
from keras.optimizers import Adam
from keras import backend as K

def meanDNN(Inputs,nclasses,nregclasses,dropoutRate=None,batchNorm=True,splitInputs=False,pfix='',arch='30x15x5'):
    """mean regression with a DNN"""

    nodes=arch.split('x')
    arch=[(i+1, int(nodes[i]), 'lrelu') for i in xrange(0,len(nodes))]

    if batchNorm:
        x=BatchNormalization(name='bn_0')(Inputs[0])
    else:
        x=Inputs[0]
    for ilayer,isize,iact in arch:                          
        x = Dense(isize,  
                  kernel_initializer='glorot_normal', 
                  bias_initializer='glorot_uniform', 
                  name='dense_%d'%ilayer)(x)
        if batchNorm:
            x = BatchNormalization(name='bn_%d'%ilayer)(x)
        if dropoutRate: 
            x = Dropout(dropoutRate)(x)
        if iact=='lrelu':
            x = LeakyReLU(0.2,name='lrelu_act_%d'%ilayer)(x)
        if iact=='prelu':
            x =  PReLU(name='prelu_act_%d'%ilayer)(x)

    if splitInputs:
        x = Dense(1, 
                  kernel_initializer='glorot_normal', 
                  bias_initializer='glorot_uniform', 
                  name='pre_mu_1')(x)
        x = Add(name='add_splitinput')( [x,Inputs[1]] )
        x = Dense(2, 
                  kernel_initializer='glorot_normal', 
                  bias_initializer='glorot_uniform', 
                  name='pre_mu_2')(x)

    x = Dense(1, use_bias=True, name='mu'+pfix)(x)
            
    return Model(inputs=Inputs, outputs=[x])

def meanpquantilesDNN(Inputs,nclasses,nregclasses,dropoutRate=None,batchNorm=True,splitInputs=False,pfix='',arch='30x15x5'):
    """model for the regression of the recoil scale"""

    nodes=arch.split('x')
    arch=[(i+1, int(nodes[i]), 'lrelu') for i in xrange(0,len(nodes))]

    param_nn={}
    for p in ['mu','qm','qp']:
        if batchNorm:
            param_nn[p]=BatchNormalization(name='bn_%s_0'%p)(Inputs[0])
        else:
            param_nn[p]=Inputs[0]
        for ilayer,isize,iact in arch:
            param_nn[p] = Dense(isize, 
                                kernel_initializer='glorot_normal', 
                                bias_initializer='glorot_uniform',
                                name='dense_%s_%d'%(p,ilayer))(param_nn[p])
            if batchNorm:
                param_nn[p]=BatchNormalization(name='bn_%s_%d'%(p,ilayer))(param_nn[p])
            if iact=='lrelu':
                param_nn[p] = LeakyReLU(0.2)(param_nn[p])

        #final layer
        param_nn[p]=Dense(1,
                          kernel_initializer='glorot_normal', 
                          bias_initializer='glorot_uniform',
                          activation='linear',
                          name=p)(param_nn[p])

    #build the final model
    output_global = concatenate([param_nn[p] for p in ['mu','qm','qp']])
    return Model(inputs=Inputs, outputs=[output_global])  

def semiParamDNN(Inputs,nclasses,nregclasses,dropoutRate=None,batchNorm=True,splitInputs=False,pfix='',arch='30x15x5'):
    """model for the regression of the recoil scale"""

    nodes=arch.split('x')
    arch=[(i+1, int(nodes[i]), 'lrelu') for i in xrange(0,len(nodes))]

    param_nn={}
    for p in ['mu','sigma','a1','a2']:
        if batchNorm:
            param_nn[p]=BatchNormalization(name='bn_%s_0'%p)(Inputs[0])
        else:
            param_nn[p]=Inputs[0]

        for ilayer,isize,iact in arch:
            param_nn[p] = Dense(isize, 
                                kernel_initializer='glorot_normal', 
                                bias_initializer='glorot_uniform',
                                name='dense_%s_%d'%(p,ilayer))(param_nn[p])
            if batchNorm:
                param_nn[p]=BatchNormalization(name='bn_%s_%d'%(p,ilayer))(param_nn[p])
            if iact=='lrelu':
                param_nn[p] = LeakyReLU(0.2)(param_nn[p])

        #final layer
        param_nn[p]=Dense(1,
                          kernel_initializer='glorot_normal', 
                          bias_initializer='glorot_uniform',
                          activation='linear',
                          name=p)(param_nn[p])

    #build the final model
    output_global = concatenate([param_nn[p] for p in ['mu','sigma','a1','a2']])
    return Model(inputs=Inputs, outputs=[output_global])       

def semiParamDNNreduced(Inputs,nclasses,nregclasses,dropoutRate=None,batchNorm=True,splitInputs=False,pfix='',arch='30x15x5'):
    """model for the regression of the recoil scale"""

    nodes=arch.split('x')
    arch=[(i+1, int(nodes[i]), 'lrelu') for i in xrange(0,len(nodes))]

    param_nn={}
    for p in ['mu','sigma']:
        if batchNorm:
            param_nn[p]=BatchNormalization(name='bn_%s_0'%p)(Inputs[0])
        else:
            param_nn[p]=Inputs[0]

        for ilayer,isize,iact in arch:
            param_nn[p] = Dense(isize,
                                kernel_initializer='glorot_normal',
				bias_initializer='glorot_uniform',
                                name='dense_%s_%d'%(p,ilayer))(param_nn[p])
            if batchNorm:
                param_nn[p]=BatchNormalization(name='bn_%s_%d'%(p,ilayer))(param_nn[p])
            if iact=='lrelu':
		param_nn[p] = LeakyReLU(0.2)(param_nn[p])

        #final layer                                                                                                                 
	param_nn[p]=Dense(1,
                          kernel_initializer='glorot_normal',
                          bias_initializer='glorot_uniform',
	                  activation='linear',
                          name=p)(param_nn[p])

    #build the final model                                                                                                           
    output_global = concatenate([param_nn[p] for p in ['mu','sigma']])
    return Model(inputs=Inputs, outputs=[output_global])


def semiParamDNNForE2(Inputs,nclasses,nregclasses,dropoutRate=None,batchNorm=True,splitInputs=False,pfix=''):
    """model for the regression of the recoil direction"""
    
    param_nn={}
    for p in ['mu_e2','sigma_e2','a1_e2','a2_e2','n_e2']:
        if batchNorm:
            param_nn[p]=BatchNormalization(name='bn_%s_0'%p)(Inputs[0])
        else:
            param_nn[p]=Inputs[0]
        for ilayer,isize,iact in [(1, 100, 'lrelu'),
                                  (2, 50,  'lrelu'),
                                  (3, 8,   'lrelu')]:
            param_nn[p] = Dense(isize, 
                                kernel_initializer='glorot_normal', 
                                bias_initializer='glorot_uniform',
                                name='dense_%s_%d'%(p,ilayer))(param_nn[p])
            if batchNorm:
                param_nn[p]=BatchNormalization(name='bn_%s_%d'%(p,ilayer))(param_nn[p])
            if iact=='lrelu':
                param_nn[p] = LeakyReLU(0.2)(param_nn[p])

        #final layer
        param_nn[p]=Dense(1,
                          kernel_initializer='glorot_normal', 
                          bias_initializer='glorot_uniform',
                          activation='linear',
                          name=p)(param_nn[p])

    #build the final model
    output_global = concatenate([param_nn[p] for p in ['mu_e2','sigma_e2','a1_e2','a2_e2','n_e2']])
    return Model(inputs=Inputs, outputs=[output_global])

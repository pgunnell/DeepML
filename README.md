# DeepML

Machine learning based on DeepJetCore 

## Installation and environment
Follow the installation instructions on https://github.com/DL4Jets/DeepJetCore. Once all is compiled 
then install these scripts to run the regression studies

```
git clone ssh://git@gitlab.cern.ch:7999/psilva/DeepML.git
cd DeepML
source lxplus_env.sh
```

## Running

Use the runRecoilScaleRegression.sh found under scripts. An example below.
The first argument is the model number (defined in Train/test_TrainData_Recoil.py)
the second argument is the output directory.
An optional third argument is the cut to apply (by default isW==0)
```
sh scripts/runRecoilRegression.sh 1 regression_results;
```

## Diagnostics

Some scripts to do basic plotting are available.
The inputs can be a directory with results or a CSV list of directories with results.
To plot the loss, mse, and all the metrics which were registered use
```
python scripts/makeTrainValidationPlots.py regression_results
```
To make some basic validation plots
```
python scripts/makePlots.py -i regression_results -o plots
```
To make a combined comparison from the validation plots
(NB this one is not automatized and requires manual editing for the moment)
```
python scripts/comparePlottedResults.py
```